# -*- coding: ISO-8859-1 -*-
import hashlib
import sys

reload(sys)
sys.setdefaultencoding('utf8')

class Generation():
    def __init__(self, login, mdp, filename):
        self.login = login
        self.mdp = mdp
        self.filename = filename

    def choix_1(self):
        for i in range(97, 123): # a to z
            for a in range(1930, 2001):
                pwd = chr(i) + self.login + str(a)
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd
        for i in range(65, 91): # A to Z
            for a in range(1930, 2001):
                pwd = chr(i) + self.login + str(a)
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd
        return 0

    def choix_2(self):
        # self.filename = "dico_animaux.txt"
        file = open(self.filename, "r")
        nbligne = 0
        while file.readline():
            nbligne += 1
        file.close()

        Words = []
        file = open(self.filename, "r")
        for i in range(0, nbligne):
            Words.append(str(file.readline()))
        file.close()

        for mot in Words:
            mot = mot[:len(mot) - 1]
            for i in range(0, 9999):
                pwd = mot.upper() + str(i)
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd

                pwd = mot.lower() + str(i)
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd

                pwd = str(i) + mot.upper()
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd

                pwd = str(i) + mot.lower()
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd
        return 0

    def choix_3(self): #lion
        self.filename = "dico_animaux.txt"
        file = open(self.filename, "r")
        nbligne = 0
        while file.readline():
            nbligne += 1
        file.close()

        Words = []
        file = open(self.filename, "r")
        for i in range(0, nbligne):
            Words.append(str(file.readline()))
        file.close()

        for mot in Words:
            mot = mot[:len(mot) - 1]
            pwd = mot
            testmdp = hashlib.md5(pwd.encode()).hexdigest()
            if testmdp == self.mdp:
                return pwd
        return 0


    def choix_4(self): #Faucon30 or 123pandas or etc
        self.filename = "dico_animaux.txt"
        file = open(self.filename, "r")
        nbligne = 0
        while file.readline():
            nbligne += 1
        file.close()

        Words = []
        file = open(self.filename, "r")
        for i in range(0, nbligne):
            Words.append(str(file.readline()))
        file.close()

        for mot in Words:
            mot = mot[:len(mot) - 1]
            for i in range(0, 999):
                pwd = mot.title() + str(i)  #Mot123
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd

                pwd = mot.lower() + str(i)  #mot123
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd

                pwd = str(i)  + mot.title()  #123Mot
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd

                pwd = str(i)  + mot.lower()  #123mot
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd
        return 0

    def choix_5(self): #S8NGL88R
        reload(sys)
        sys.setdefaultencoding('ISO-8859-1')
        self.filename = "dico_animaux.txt"
        file = open(self.filename, "r")
        nbligne = 0
        while file.readline():
            nbligne += 1
        file.close()

        Words = []
        file = open(self.filename, "r")
        for i in range(0, nbligne):
            Words.append(str(file.readline()))
        file.close()

        lettre = ""
        pwd = ""
        for mot in Words:
            mot = mot[:len(mot) - 1]
            word = mot.upper()
            for n in range(0, 10):
                for l in word:
                    if (l == 'A' or l == 'E' or l == '�' or l == '�' or l == 'I' or l == 'O' or l == 'U' or l == 'Y'):
                        l = str(n)
                        lettre += l
                    else:
                        lettre += l
                pwd = lettre
                lettre = ""
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd
        return 0

    def choix_6(self): #dranacdranac
        reload(sys)
        sys.setdefaultencoding('ISO-8859-1')
        self.filename = "dico_animaux.txt"
        file = open(self.filename, "r")
        nbligne = 0
        while file.readline():
            nbligne += 1
        file.close()

        Words = []
        file = open(self.filename, "r")
        for i in range(0, nbligne):
            Words.append(str(file.readline()))
        file.close()
        motInverse = ""
        for mot in Words:
            mot = mot[:len(mot) - 1]
            for lettre in reversed(mot):
                motInverse += lettre
            motInverse += motInverse
            pwd = motInverse
            motInverse = ""
            testmdp = hashlib.md5(pwd.encode()).hexdigest()
            if testmdp == self.mdp:
                return pwd
        return 0

    def choix_7(self): #morsetigre
        self.filename = "dico_animaux.txt"
        file = open(self.filename, "r")
        nbligne = 0
        while file.readline():
            nbligne += 1
        file.close()

        Words1 = []
        Words2 = []
        file1 = open(self.filename, "r")
        file2 = open(self.filename, "r")
        for i in range(0, nbligne):
            Words1.append(str(file1.readline()))
            Words2.append(str(file2.readline()))
        file1.close()
        file2.close()

        for mot1 in Words1:
            mot1 = mot1[:len(mot1) - 1]
            for mot2 in Words2:
                mot2 = mot2[:len(mot2) - 1]
                pwd = mot1 + mot2
                testmdp = hashlib.md5(pwd.encode()).hexdigest()
                if testmdp == self.mdp:
                    return pwd
        return 0


