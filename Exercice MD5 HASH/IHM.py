# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import filedialog
from Generation import *

class Interface(Frame):
    def __init__(self, window):
        Frame.__init__(self, window)
        window.title("Decryptage de mdp")
        window.resizable(width = FALSE, height = FALSE)
        window.geometry('600x500')

        Frame_1 = Frame(window)
        Frame_1.pack(side = TOP, padx = 0, pady = 0)

        self.login = Label(Frame_1, text = "Login").pack(side = LEFT)
        self.elogin = Entry(Frame_1)
        self.elogin.pack(side = LEFT)
        self.password = Label(Frame_1, text = "Password").pack(side = LEFT)
        self.epassword = Entry(Frame_1)
        self.epassword.pack(side = LEFT)

        Frame_2 = Frame(window)
        Frame_2.pack(side = LEFT, padx =20 , pady = 0)

        self.choix = IntVar()
        b1 = Radiobutton(Frame_2, text = "Lettre + Login + Annee Naissance", fg = "red", variable = self.choix, value = 1).pack(anchor = W)
        b2 = Radiobutton(Frame_2, text = "animal + chiffres (max 4)", variable = self.choix, value = 2).pack(anchor = W)
        l4 = Label(Frame_2, text="Saisir le nom fichier.").pack()
        self.file = Entry(Frame_2)
        self.file.pack()
        b3 = Radiobutton(Frame_2, text = "Classique", variable = self.choix, value = 3).pack(anchor = W)
        b4 = Radiobutton(Frame_2, text = "Nombres devant ou derrière + maj au premier lettre ou non", variable = self.choix, value = 4).pack(anchor = W)
        b5 = Radiobutton(Frame_2, text = "Voyelle = chiffre et toutes en majuscule", variable = self.choix, value = 5).pack(anchor = W)
        b6 = Radiobutton(Frame_2, text = "Animal à l'envers et dédoublé", variable = self.choix, value = 6).pack(anchor = W)
        b7 = Radiobutton(Frame_2, text = "Concaténation de 2 animaux", variable = self.choix, value = 7).pack(anchor = W)


        self.Search = Button(Frame_2, text="Chercher", command=self.search)
        self.Search.pack(pady = 10)

        self.re = Label(Frame_2, text = "Résultat : ")
        self.re.pack(anchor= W, padx = 10, pady = 0)
        self.result = Label(Frame_2, text = "", fg="blue")
        self.result.pack(anchor="e")
        self.message = Label(Frame_2, text = "", fg="red")
        self.message.pack(anchor="e")

        Frame_3 = Frame(window)
        Frame_3.pack(side = BOTTOM, padx = 5, pady = 10, anchor = W)

        self.b_quit = Button(Frame_3, text = "Quitter", command = self.quit)
        self.b_quit.pack(padx = 30)

    def search(self):
        c = Generation(self.elogin.get(), self.epassword.get(), self.file.get())

        a = self.choix.get()
        res = 0

        if a == 0:
            res = "Choisissez une option"
        elif a == 1:
            res = c.choix_1()
        elif a == 2:
            if self.file.get() == "":
                res = 0
            else:
                res = c.choix_2()
        elif a == 3:
            res = c.choix_3()
        elif a == 4:
            res = c.choix_4()
        elif a == 5:
            res = c.choix_5()
        elif a == 6:
            res = c.choix_6()
        elif a == 7:
            res = c.choix_7()

        if res == 0:
            self.message["text"] = "Saisir un fichier !"
            self.result["text"] = "Décryptage impossible"
        else:
            self.message["text"] = ""
            self.result["text"] = res
        return 0





