package graphe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Principal {
	public static boolean recherche(char a, char b, List<Arete> li) {
		int i =0;

		if(a==b) {
			return true;
		}

		if(li.size()<2) {
			return false;
		}

		for(Arete x : li) {
			char xO = x.getOrigine();
			char xD = x.getDestination();
			if(a==xO && b==xD) {
				return true;
			}
			if(xO == a) {
				List<Arete> li2 = new ArrayList<Arete>(li);
				li2.remove(i);
				if(recherche(xD,b, li2)) {
					return true;
				}
			}
			if(xD == a) {
				List<Arete> li2 = new ArrayList<Arete>(li);
				li2.remove(i);
				if(recherche(xO,b, li2)) {
					return true;
				}
			}
			i++;
		}
		return false;
	}

	public static void main(String[] args) 
	{
		ArrayList<Arete> arete = new ArrayList<Arete>(); 
		arete.add(new Arete ('A','B',6));
		arete.add(new Arete ('A','C',3));
		arete.add(new Arete ('B','C',6));
		arete.add(new Arete ('C','D',2));
		arete.add(new Arete ('D','A',2));
		arete.add(new Arete ('B','G',6));		
		arete.add(new Arete ('G','F',8));
		arete.add(new Arete ('G','A',7));
		arete.add(new Arete ('F','E',1));
		arete.add(new Arete ('E','D',3));		
		arete.add(new Arete ('E','A',4));
		arete.add(new Arete ('F','A',3));

		Collections.sort(arete);

		List<Arete> resultat = new ArrayList<Arete>();
		for(Arete a : arete) {
			if(recherche(a.getOrigine(), a.getDestination(), resultat)==false ){
				resultat.add(a);
			}
		}

		System.out.println("Les aretes de l'arbre couvrant minimum sont : \n");
		for(Arete a: resultat) {
			System.out.println(a.toString());
		}
	}
}
