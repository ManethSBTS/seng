<?php
session_start();

try
{
	$bdd = new PDO('mysql:host=localhost;dbname=TPPHP;charset=utf8', 'root', 'root');
}
catch(Exception $e)
{
  die('Erreur : '.$e->getMessage());
}

$sql = "SELECT id FROM acces WHERE login = :login AND password = :password";
$result = $bdd->prepare($sql);
$result->bindParam(':login', $_POST['username'], PDO::PARAM_STR);
$result->bindParam('password', $_POST['password'], PDO::PARAM_STR);
$result->execute();
$rep = $result->rowCount();

if($rep == 1){
  $_SESSION['login'] = $_POST['username'] ;
  $_SESSION['CONNECTE'] = 'YES' ;
  header ('location:liste.php');
}else {
  header ('location:login.php?message=Erreur');
}
?>
