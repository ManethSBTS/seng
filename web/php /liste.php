<?php
session_start();
if ($_SESSION['CONNECTE'] != 'YES'){header ('location:login.php?connexion=Erreur');}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <style media="screen">
    .img {
      cursor: pointer;
    }

    .pp {
      font-size: 18px;
      margin-left: 1%;
    }

    body {
      text-align: center;
    }

    #tab {
      margin-left: 43%;
      height: 30%;
    }

  </style>
  <body>
    <p class="pp"><?php echo $_SESSION['login']?></p>
    <a href="deconnexion.php"><img src="images/deconnexion.png" alt="deconnexion"/> Déconnexion</a>
    <br>
    <br>
    <a href="ajoute.php"><img src="images/ajoute.png" alt="ajouter"/> Ajouter</a>
    <br>
    <br>
    <?php
    try
    {
	     $bdd = new PDO('mysql:host=localhost;dbname=TPPHP;charset=utf8', 'root', 'root');
     }
     catch(Exception $e)
     {
       die('Erreur : '.$e->getMessage());
     }

     $sql = 'SELECT * FROM Acces';
     $result = $bdd->query($sql);
     ?>
     <table id="tab">
       <tr>
         <th>id</th>
         <th>prenom</th>
         <th>login</th>
         <th>statut</th>
         <th>age</th>
         <?php
         while ($row = $result->fetch())
         {
           ?>
         </tr>
         <tr>
           <td><?php echo $row['id']; ?></td>
           <td><?php echo $row['prenom']; ?></td>
           <td><?php echo $row['login']; ?></td>
           <td><?php echo $row['statut']; ?></td>
           <td><?php echo $row['age']; ?></td>
           <td><a href="modif.php?id=<?php echo $row['id'];?>&&prenom=<?php echo $row['prenom']; ?>&&login=<?php echo $row['login']; ?>&&password=<?php echo $row['password']; ?>&&statut=<?php echo $row['statut']; ?>&&age=<?php echo $row['age']; ?>"><img class="img" src="images/modif.png" alt="UPDATE"></a></td>
           <td><a href="efface.php?id=<?php echo $row['id']; ?>"><img class="img" src="images/croix.png" alt="DELETE"></a></td>
         </tr>
         <?php
       }
       echo '</table>';
       $reponse->closeCursor();
       ?>
  </body>
</html>
