<?php
session_start();
if ($_SESSION['CONNECTE'] != 'YES'){header ('location:login.php?connexion=Erreur');}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <style media="screen">
    body {
      text-align: center;
    }

    #tab {
      margin-left: 43%;
      height: 30%;
    }

    .pp {
      font-size: 18px;
      margin-left: 1%;
    }
  </style>
  <body>
    <p class="pp"><?php echo $_SESSION['login']?></p>
    <a href="deconnexion.php"><img src="images/deconnexion.png" alt="deconnexion"/> Déconnexion</a>
    <br>
    <br>
    <div class="">
      <form class="" action="#" method="post">
        <h2>Modification</h2>
        <table id="tab">
          <tr>
            <td>Prenom</td>
            <td><input type="text" name="prenom" value="<?php echo $_GET["prenom"]; ?>" required></td>
          </tr>
          <tr>
            <td>Login</td>
            <td><input type="text" name="login" value="<?php echo $_GET["login"]; ?>"></td>
          </tr>
          <tr>
            <td>Password</td>
            <td><input type="text" name="password" value="<?php echo $_GET["password"]; ?>"></td>
          </tr>
          <tr>
            <td>Statut</td>
            <td>
              <select type="text" class="form" name="statut" value="<?php echo $_GET["statut"]; ?>">
              <?php
              try
              {
              	$bdd = new PDO('mysql:host=localhost;dbname=TPPHP;charset=utf8', 'root', 'root');
              }
              catch(Exception $e)
              {
                die('Erreur : '.$e->getMessage());
              }

              $sql = 'SELECT nom FROM statut';
              $result = $bdd->query($sql);

              while($row = $result->fetch()) {
                echo "<option>" . $row["nom"] . "</option>";
              }
               ?>
            </select>
            </td>
          </tr>
          <tr>
            <td>Age</td>
            <td><input type="text" name="age" value="<?php echo $_GET["age"]; ?>"></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" name="update" value="Modifier"></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" name="return" value="Retour à la liste"></td>
          </tr>
        </table>
      </form>
    </div>
  </body>
</html>

<?php
if(isset($_POST['update'])){
  try
  {
  	$bdd = new PDO('mysql:host=localhost;dbname=TPPHP;charset=utf8', 'root', 'root');
  }
  catch(Exception $e)
  {
    die('Erreur : '.$e->getMessage());
  }

  $sql = "UPDATE acces SET prenom = :prenom, login = :login, password = :password, statut = :statut, age = :age WHERE id = :id";
  $result = $bdd->prepare($sql);
  $result->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
  $result->bindParam(':prenom', $_POST['prenom'], PDO::PARAM_STR);
  $result->bindParam(':login', $_POST['login'], PDO::PARAM_STR);
  $result->bindParam(':password', $_POST['password'], PDO::PARAM_STR);
  $result->bindParam(':statut', $_POST['statut'], PDO::PARAM_STR);
  $result->bindParam(':age', $_POST['age'], PDO::PARAM_STR);
  $result->execute();
  header ('location:liste.php?modif=Modifier');
}
else if(isset($_POST['return'])){
  header ('location:liste.php?ajoute=Ajouter');
}
?>
