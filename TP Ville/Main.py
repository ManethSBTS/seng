from ClassVille import *
from ClassCapitale import *

v1 = Ville("toulouse")
v2 = Ville("strasbourg", 272975)

print(v1)
print(v2)
print("")

c1 = Capitale("paris", "france")
c2 = Capitale("rome", "italie", 2700000)

print(c1)
print(c2)
print("")

print("Catégorie de le ville de "+ v1.get_Nom() + " : " + v1.categorie())
print("Catégorie de le ville de "+ v2.get_Nom() + " : " + v2.categorie())
print("Catégorie de le ville de "+ c1.get_Nom() + " : " + c1.categorie())