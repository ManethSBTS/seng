from ClassVille import *

class Capitale(Ville):
    def __init__(self, nom, nom_pays, nbHabitants = 0):
        super().__init__(nom, nbHabitants)
        self.nom_p = nom_pays.upper()

    def get_N_Pays(self):
        return self.nnom_p

    def set_N_Pays(self, n_n_pays):
        self.nom_p = n_n_pays

    def __str__(self):
        return Ville.__str__(self) + " / Capitale de " + self.nom_p

    def nbHabitantsConnu(self):
        return Ville.nbHabitantsConnu(self)

    def categorie(self):
        return "C"