class Ville:
    def __init__(self, nom, nbHabitants = 0):
        self.n = nom.upper()
        self.nbH = nbHabitants

    #region Getter & Setter

    def get_Nom(self):
        return self.n

    #def set_Nom(self, n_nom):
    #    self.n = n_nom

    def get_NbHabitants(self):
        return self.nbH

    def set_NbHabitants(self, nb_hab):
        self.nbH = nb_hab

    #endregion

    def nbHabitantsConnu(self):
        if (self.get_NbHabitants() > 0):
            return True
        else:
            return False

    def __str__(self):
        return "Nom de la Ville est " + self.n + " / Nombre d'habitants est " + str(self.nbH)

    def categorie(self):
        nb = 500000
        if ((self.get_NbHabitants() > 0) and (self.get_NbHabitants() < nb)):
            result = "A"
        elif (self.get_NbHabitants() >= nb):
            result = "B"
        else:
            result = "?"
        return result